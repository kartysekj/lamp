/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

/* načtení knihoven */
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>
#include "ledky.h"
#include <Wire.h>
#include "LittleFS.h"
//#include <ESP8266mDNS.h>
#include <FastLED.h> 
#define LED_PIN     D8
#include "DHT.h"
#define DHTPIN D9
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

//prihlaseni na wifi
const char *ssid = "Mrak_Net_Kartysek";
const char *password = "0607673059";

/* nastavení konstant periferií */
const int led_deska = LED_BUILTIN;
int i = 0;

/* stav LED */
char* ledState;

/* vytvoření instance AsyncWebServer na portu 80 */
AsyncWebServer server(80);

/* ukázka get metody, vrací náhodné číslo <0;100> */
String getRandom() {
  int nahodne = random(1000);
  Serial.println(nahodne);
  return String(nahodne);
}

/* nastavení String ke zpětnému načtení do frontend */
String processor(const String& var){
  Serial.println(var);
  if(var == "STAV"){
    if(digitalRead(led_deska)){
      ledState = "ON";
    }
    else{
      ledState = "OFF";
    }
    Serial.print(ledState);
    return ledState;
  }
  else if (var == "LIGHT"){
    return getLight();
  }
  else if (var == "DRY"){
    return getDry();
  }
  else if (var == "TEMP"){
    return getTemp();
  }
  return "";
}
 String getLight() {
  int svetlo = analogRead(A0);;
  Serial.println(svetlo);
  return String(svetlo);
 }
/* String getTemp(){
  int teplota = random(19,22);
  Serial.println(teplota);
  return String(teplota);
 }
 String getDry() {
  int vlhkost = random(35,41);
  Serial.println(vlhkost);
  return String(vlhkost);
 }
 */
String getTemp(){
  int teplota = dht.readTemperature();
  Serial.println(teplota);
  delay(500);
  return String(teplota);
 }
 String getDry(){
  int vlhkost = dht.readHumidity();
  Serial.println(vlhkost);
  delay(500);
  return String(vlhkost);
 }
/* funkce přepne stav LED na desce a zavolá handleRoot() */
void zmenabarvy() {
  i++;  
}


void setup(){
  /* debug */
  Serial.begin(9600);
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  /* nastavení LED_BUILTIN */
  pinMode(led_deska, OUTPUT);
  digitalWrite(led_deska, LOW);
  dht.begin();
/*led pasek*/

  strip.begin();
  strip.show();
  strip.setBrightness(50);
  
  /* připojení LittleFS */
  if(!LittleFS.begin()){
    Serial.println("Při připojování LittleFS vznikla chyba");
    return;
  }
  WiFi.begin(ssid, password);
  Serial.println("");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("\nPřipojeno na: ");
  Serial.println(ssid);
  Serial.print("IP adresa: ");
  Serial.println(WiFi.localIP());
  /* spuštění Multicast DNS - přiděluji adresy ke jménům v malé síti */
  /* ======== IMPORTANT ======== */
  /* nastavení přístupových bodů a s nimi svázaných funkcí */
  /* root / */
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(LittleFS, "/index.html", String(), false, processor);
  });
  /* /style.css */
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(LittleFS, "/style.css", "text/css");
  });
  /* /prepni - změna stavu LED */
  server.on("/prepni", HTTP_GET, [](AsyncWebServerRequest *request){
    zmenabarvy();
    request->send(LittleFS, "/index.html", String(), false, processor);
  });
  //random - načtení náhodné hodnoty 
  server.on("/light", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getLight().c_str());
  });
  server.on("/temp", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getTemp().c_str());
  });
  server.on("/dry", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getDry().c_str());
  });



  /* ======== IMPORTANT ======== */

  /* po nastavení můžeme spustit server a vypsat hlášku do konzole */
 server.begin();
}
 
void loop(){
 switch (i){
 case 0:
    tma();
    break;
  case 1:
    red();
    break;
  case 2:
    green();
    break;
  case 3:
   blue();
    break;
  case 4:
    yellow();
    break;
  case 5:
    lightblue();
    break;
    case 6:
    purple();
    break;
  case 7:
    mix1();
  break;
  case 8:
    rainbow(10);
    break;
  default:
    i = 0;
    break;
  }
}
