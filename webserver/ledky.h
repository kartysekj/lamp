#include <FastLED.h> 
#include <Adafruit_NeoPixel.h>
#define NUM_LEDS    19
#define LED_PIN     D8
Adafruit_NeoPixel strip(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);

CRGB leds[NUM_LEDS];
void tma(){
  leds[0] = CRGB(0, 0, 0);
  FastLED.show();
  leds[1] = CRGB(0, 0, 0);
  FastLED.show();
  leds[2] = CRGB(0, 0, 0);
  FastLED.show();
  leds[3] = CRGB(0, 0, 0);
  FastLED.show();
  leds[4] = CRGB(0, 0, 0);
  FastLED.show();
  leds[5] = CRGB(0, 0, 0);
  FastLED.show();
  leds[6] = CRGB(0, 0, 0);
  FastLED.show();
  leds[7] = CRGB(0, 0, 0);
  FastLED.show();
  leds[8] = CRGB(0, 0, 0);
  FastLED.show();
  leds[9] = CRGB(0, 0, 0);
  FastLED.show();
  leds[10] = CRGB(0, 0, 0);
  FastLED.show();
  leds[11] = CRGB(0, 0, 0);
  FastLED.show();
  leds[12] = CRGB(0, 0, 0);
  FastLED.show();
  leds[13] = CRGB(0, 0, 0);
  FastLED.show();
  leds[14] = CRGB(0, 0, 0);
  FastLED.show();
  leds[15] = CRGB(0, 0, 0);
  FastLED.show();
  leds[16] = CRGB(0, 0, 0);
  FastLED.show();
  leds[17] = CRGB(0, 0, 0);
  FastLED.show();
  leds[18] = CRGB(0, 0, 0);
  FastLED.show();
  leds[19] = CRGB(0, 0, 0);
  FastLED.show();
}
void red()  {
  leds[0] = CRGB(255, 0, 0);
  FastLED.show();  
  leds[1] = CRGB(255, 0, 0);
  FastLED.show();
  leds[2] = CRGB(255, 0, 0);
  FastLED.show();
  leds[3] = CRGB(255, 0, 0);
  FastLED.show();
  leds[4] = CRGB(255, 0, 0);
  FastLED.show();
  leds[5] = CRGB(255, 0, 0);
  FastLED.show();
  leds[6] = CRGB(255, 0, 0);
  FastLED.show();
  leds[7] = CRGB(255, 0, 0);
  FastLED.show();
  leds[8] = CRGB(255, 0, 0);
  FastLED.show();
  leds[9] = CRGB(255, 0, 0);
  FastLED.show();
  leds[10] = CRGB(255, 0, 0);
  FastLED.show();
  leds[11] = CRGB(255, 0, 0);
  FastLED.show();
  leds[12] = CRGB(255, 0, 0);
  FastLED.show();
  leds[13] = CRGB(255, 0, 0);
  FastLED.show();
  leds[14] = CRGB(255, 0, 0);
  FastLED.show();
  leds[15] = CRGB(255, 0, 0);
  FastLED.show();
  leds[16] = CRGB(255, 0, 0);
  FastLED.show();
  leds[17] = CRGB(255, 0, 0);
  FastLED.show();
  leds[18] = CRGB(255, 0, 0);
  FastLED.show();
  leds[19] = CRGB(255, 0, 0);
  FastLED.show();
}
void green(){
  leds[0] = CRGB(0, 255, 0);
  FastLED.show(); 
  leds[1] = CRGB(0, 255, 0);
  FastLED.show();
  leds[2] = CRGB(0, 255, 0);
  FastLED.show();
  leds[3] = CRGB(0, 255, 0);
  FastLED.show();
  leds[4] = CRGB(0, 255, 0);
  FastLED.show();
  leds[5] = CRGB(0, 255, 0);
  FastLED.show();
  leds[6] = CRGB(0, 255, 0);
  FastLED.show();
  leds[7] = CRGB(0, 255, 0);
  FastLED.show();
  leds[8] = CRGB(0, 255, 0);
  FastLED.show();
  leds[9] = CRGB(0, 255, 0);
  FastLED.show();
  leds[10] = CRGB(0, 255, 0);
  FastLED.show();
  leds[11] = CRGB(0, 255, 0);
  FastLED.show();
  leds[12] = CRGB(0, 255, 0);
  FastLED.show();
  leds[13] = CRGB(0, 255, 0);
  FastLED.show();
  leds[14] = CRGB(0, 255, 0);
  FastLED.show();
  leds[15] = CRGB(0, 255, 0);
  FastLED.show();
  leds[16] = CRGB(0, 255, 0);
  FastLED.show();
  leds[17] = CRGB(0, 255, 0);
  FastLED.show();
  leds[18] = CRGB(0, 255, 0);
  FastLED.show();
  leds[19] = CRGB(0, 255, 0);
  FastLED.show();
}
void blue(){
  leds[0] = CRGB(0, 0, 255);
  FastLED.show(); 
  leds[1] = CRGB(0, 0, 255);
  FastLED.show();
  leds[2] = CRGB(0, 0, 255);
  FastLED.show();
  leds[3] = CRGB(0, 0, 255);
  FastLED.show();
  leds[4] = CRGB(0, 0, 255);
  FastLED.show();
  leds[5] = CRGB(0, 0, 255);
  FastLED.show();
  leds[6] = CRGB(0, 0, 255);
  FastLED.show();
  leds[7] = CRGB(0, 0, 255);
  FastLED.show();
  leds[8] = CRGB(0, 0, 255);
  FastLED.show();
  leds[9] = CRGB(0, 0, 255);
  FastLED.show();
  leds[10] = CRGB(0, 0, 255);
  FastLED.show();
  leds[11] = CRGB(0, 0, 255);
  FastLED.show();
  leds[12] = CRGB(0, 0, 255);
  FastLED.show();
  leds[13] = CRGB(0, 0, 255);
  FastLED.show();
  leds[14] = CRGB(0, 0, 255);
  FastLED.show();
  leds[15] = CRGB(0, 0, 255);
  FastLED.show();
  leds[16] = CRGB(0, 0, 255);
  FastLED.show();
  leds[17] = CRGB(0, 0, 255);
  FastLED.show();
  leds[18] = CRGB(0, 0, 255);
  FastLED.show();
  leds[19] = CRGB(0, 0, 255);
  FastLED.show();
}
void purple(){
  leds[0] = CRGB(255, 0, 255);
  FastLED.show();  
  leds[1] = CRGB(255, 0, 255);
  FastLED.show();
  leds[2] = CRGB(255, 0, 255);
  FastLED.show();
  leds[3] = CRGB(255, 0, 255);
  FastLED.show();
  leds[4] = CRGB(255, 0, 255);
  FastLED.show();
  leds[5] = CRGB(255, 0, 255);
  FastLED.show();
  leds[6] = CRGB(255, 0, 255);
  FastLED.show();
  leds[7] = CRGB(255, 0, 255);
  FastLED.show();
  leds[8] = CRGB(255, 0, 255);
  FastLED.show();
  leds[9] = CRGB(255, 0, 255);
  FastLED.show();
  leds[10] = CRGB(255, 0, 255);
  FastLED.show();
  leds[11] = CRGB(255, 0, 255);
  FastLED.show();
  leds[12] = CRGB(255, 0, 255);
  FastLED.show();
  leds[13] = CRGB(255, 0, 255);
  FastLED.show();
  leds[14] = CRGB(255, 0, 255);
  FastLED.show();
  leds[15] = CRGB(255, 0, 255);
  FastLED.show();
  leds[16] = CRGB(255, 0, 255);
  FastLED.show();
  leds[17] = CRGB(255, 0, 255);
  FastLED.show();
  leds[18] = CRGB(255, 0, 255);
  FastLED.show();
  leds[19] = CRGB(255, 0, 255);
  FastLED.show();
}
void lightblue(){
 leds[0] = CRGB(0, 255, 255);
  FastLED.show(); 
  leds[1] = CRGB(0, 255, 255);
  FastLED.show();
  leds[2] = CRGB(0, 255, 255);
  FastLED.show();
  leds[3] = CRGB(0, 255, 255);
  FastLED.show();
  leds[4] = CRGB(0, 255, 255);
  FastLED.show();
  leds[5] = CRGB(0, 255, 255);
  FastLED.show();
  leds[6] = CRGB(0, 255, 255);
  FastLED.show();
  leds[7] = CRGB(0, 255, 255);
  FastLED.show();
  leds[8] = CRGB(0, 255, 255);
  FastLED.show();
  leds[9] = CRGB(0, 255, 255);
  FastLED.show();
  leds[10] = CRGB(0, 255, 255);
  FastLED.show();
  leds[11] = CRGB(0, 255, 255);
  FastLED.show();
  leds[12] = CRGB(0, 255, 255);
  FastLED.show();
  leds[13] = CRGB(0, 255, 255);
  FastLED.show();
  leds[14] = CRGB(0, 255, 255);
  FastLED.show();
  leds[15] = CRGB(0, 255, 255);
  FastLED.show();
  delay(500);
  leds[16] = CRGB(0, 255, 255);
  FastLED.show();
  leds[17] = CRGB(0, 255, 255);
  FastLED.show();
  leds[18] = CRGB(0, 255, 255);
  FastLED.show();
  leds[19] = CRGB(0, 255, 255);
  FastLED.show();
}
void yellow(){
  leds[0] = CRGB(255, 255, 0);
  FastLED.show(); 
  leds[1] = CRGB(255, 255, 0);
  FastLED.show();
  leds[2] = CRGB(255, 255, 0);
  FastLED.show();
  leds[3] = CRGB(255, 255, 0);
  FastLED.show();
  leds[4] = CRGB(255, 255, 0);
  FastLED.show();
  leds[5] = CRGB(255, 255, 0);
  FastLED.show();
  leds[6] = CRGB(255, 255, 0);
  FastLED.show();
  leds[7] = CRGB(255, 255, 0);
  FastLED.show();
  leds[8] = CRGB(255, 255, 0);
  FastLED.show();
  leds[9] = CRGB(255, 255, 0);
  FastLED.show();
  leds[10] = CRGB(255, 255, 0);
  FastLED.show();
  leds[11] = CRGB(255, 255, 0);
  FastLED.show();
  leds[12] = CRGB(255, 255, 0);
  FastLED.show();
  leds[13] = CRGB(255, 255, 0);
  FastLED.show();
  leds[14] = CRGB(255, 255, 0);
  FastLED.show();
  leds[15] = CRGB(255, 255, 0);
  FastLED.show();
  leds[16] = CRGB(255, 255, 05);
  FastLED.show();
  leds[17] = CRGB(255, 255, 0);
  FastLED.show();
  leds[18] = CRGB(255, 255, 0);
  FastLED.show();
  leds[19] = CRGB(255, 255, 0);
  FastLED.show();
}
void mix1(){
  
  for (int p = 0; p <= 19; p++) {
    leds[p] = CRGB ( 0, 0, 255);
    FastLED.show();
    delay(40);
    }
  for (int p = 19; p >= 0; p--) {
    leds[p] = CRGB ( 255, 0, 0);
    FastLED.show();
    delay(40);
  }
}
void mix2(){
  
}
void rainbow(int wait) {
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {
    for(int i=0; i<strip.numPixels(); i++) { 
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show();
    delay(wait);
  }
}
