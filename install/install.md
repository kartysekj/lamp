# Instalace nabídky Little-FS do Arduino IDE
> umožňuje nahrávání dat do SPI paměti na ESP
[Návod k instalaci Little-FS na ESP8266](https://randomnerdtutorials.com/install-esp8266-nodemcu-littlefs-arduino/)

Po správné instalaci vypadá rozhraní Arduino IDE takto ![upravené Arduino IDE](../main/screenshots/arduinoIDE-nabidkaLittleFS.png)

Soubory, které chceme vložit do FS se vkládají do podsložky `/data` ve složce s kódem.

## Přehled souborů
| archiv    | info  |
| ---       | ---   |
| (ESP8266LittleFS-2.6.0) | přidání nabídky `LittleFS` do Arduino IDE |
| (ESPAsyncTCP-master) | knihovna pro async TCP |
| (ESPAsyncWebServer-master) | knihovna pro async webserver | 
